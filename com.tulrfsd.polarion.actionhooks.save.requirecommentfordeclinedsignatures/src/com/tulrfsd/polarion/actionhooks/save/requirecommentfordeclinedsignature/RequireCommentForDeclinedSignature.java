package com.tulrfsd.polarion.actionhooks.save.requirecommentfordeclinedsignature;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import com.adva.polarion.actionhook.IActionHook;
import com.polarion.alm.tracker.model.ICommentBase;
import com.polarion.alm.tracker.model.IModule;
import com.polarion.alm.tracker.model.signatures.IDocumentWorkflowSignature;
import com.polarion.alm.tracker.model.signatures.IDocumentWorkflowSignaturesManager;
import com.polarion.alm.tracker.model.signatures.ISignature;
import com.polarion.alm.tracker.model.signatures.ISignatureStateOpt;
import com.polarion.alm.tracker.model.signatures.ISignatureVerdictOpt;

public class RequireCommentForDeclinedSignature implements IActionHook {
  
  public static final String HOOK_ID = "RequireCommentForDeclinedSignature";
  public static final String HOOK_DESCRIPTION = "Users must provide a signature comment when declining a document signature";
  private static final String HOOK_VERSION = "1.1.0";
  
  private HashSet<String> hookProperties = new HashSet<>();

  @Override
  public String processAction(IModule module) {
    
    if (!this.hookProperties.contains(module.getProjectId()) &&
        !this.hookProperties.contains(module.getProjectId() + "." +  module.getType().getId())) {
      return null;
    }
    
    IDocumentWorkflowSignaturesManager signaturesManager = module.getWorkflowSignaturesManager();
    List<IDocumentWorkflowSignature> workflowSignatureList = signaturesManager.getSortedWorkflowSignatures();
    if (workflowSignatureList.isEmpty()) {
      return null;
    }
    
    IDocumentWorkflowSignature workflowSignature = workflowSignatureList.get(workflowSignatureList.size() - 1);
    if (workflowSignature.getSignatureState().getId().equals(ISignatureStateOpt.OPT_OBSOLETE) ||
        workflowSignature.isClosed()) {
      return null;
    }
    
    List<ISignature> signatureList = workflowSignature.getSignatures();
    for (ISignature signature : signatureList) {
      if (!signature.isModified() || !signature.getVerdict().getId().equals(ISignatureVerdictOpt.OPT_DECLINED)) {
        continue;
      }
      ICommentBase<?> verdictComment = signature.getVerdictComment();
      if (verdictComment == null || verdictComment.isUnresolvable() || verdictComment.getText().getContent().isEmpty()) {
        return "When declining a document signature, a signature comment must be provided";
      }
    }
    return null;
  }

  @Override
  public ACTION_TYPE getAction() {
    return ACTION_TYPE.SAVE;
  }

  @Override
  public String getName() {
    return HOOK_ID;
  }

  @Override
  public String getDescription() {
    return HOOK_DESCRIPTION;
  }

  @Override
  public String getVersion() {
    return HOOK_VERSION;
  }

  @Override
  public void init() {
    // no init required
  }

  @Override
  public void initProperties(Properties properties) {
    this.hookProperties.clear();
    
    if (properties == null) {
      return;
    }
    
    // read all properties from the global file belonging to this hook
    for (String propertyKey : properties.stringPropertyNames()) {
      String property = properties.getProperty(propertyKey);
      if (propertyKey.equals(this.getName()) && !property.isBlank()) {
        List<String> propertyList = Arrays.asList(property.split(",\\s*"));
        this.hookProperties.addAll(propertyList);
      }
    }  
  }

}
