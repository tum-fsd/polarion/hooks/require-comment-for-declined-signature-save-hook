# Require Comment For Declined Signature Save Hook

The latest jar file is available in the [releases section](../../releases).

This is the legacy implementation of the hook compatible with the [Work Item Action Interceptor Framework](https://extensions.polarion.com/extensions/146-work-item-action-interceptor-framework). For the new version compatible with the [Interceptor Manager](https://github.com/SchweizerischeBundesbahnen/ch.sbb.polarion.extension.interceptor-manager) extension by SBB, use the new [com.tulrfsd.polarion.interceptor.requirecommentfordeclinedsignature](../../../../com.tulrfsd.polarion.interceptor.requirecommentfordeclinedsignature) hook.

## Introduction

When users perform a LiveDoc workflow signature, they can optionally provide a comment with the signature. Especially for declined signatures, it is useful to provide a reasoning in the comment why the signature was declined. This save hook prevents users from declining a LiveDoc signature without providing such comment. When they try to save the LiveDoc, the hook will abort the action and give the users the chance to provide the missing comment. The hook can be configured for complete projects or only for specific document types of a project.

## License

Download and use of the tool is free. By downloading the tool, you accept the license terms.

See [NOTICE.md](NOTICE.md) for licensing details.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for contribution guidelines.

## Download & Installation

### Installation:

1. Have the Work Item Action Interceptor Framework (see [Installation Requirements](#Installation-Requirements)) installed and follow the instructions of that extension.

1. Go to the [releases section](../../releases) of this repository and download the latest version of the hook.

1. Copy the jar file in the `hooks` folder of the interceptor framework on your Polarion server.

1. Add the [configuration properties](#Configuration-Properties) in the `hooks.properties` file of the interceptor framework.

1. Follow the instructions of the interceptor framework on how to reload the hooks and configuration properties.

### Configuration Properties:

This hook has the following syntax for the configuration properties:

`RequireCommentForDeclinedSignature=ProjectId, ProjectId.DocumentTypeId`

You can either configure the hook for a complete project, or only for a specific document type in a project. Multiple entries are separated by a comma.

#### Syntax Examples:

`RequireCommentForDeclinedSignature=drivepilot, elibrary.generic`

### Uninstall:

- Remove the jar file from the `hooks` folder of the interceptor framework, remove the corresponding entries in the `hooks.properties` file and reload the hooks and properties in the `Action Interceptor Manager` wiki page.

### Installation Requirements:

- [Work Item Action Interceptor Framework](https://extensions.polarion.com/extensions/146-work-item-action-interceptor-framework) version 3.1.1 and later

- Polarion version 3.20.1 and later